import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {NewTodoComponent} from './new-todo.component';
import {TodoListComponent} from './todo-list.component';
import {TodoItemComponent} from './todo-item.component';
import {AboutContainer} from './about.container';
import {RouterModule} from '@angular/router';
import {TodoContainer} from './todo.container';
import {TodoService} from './todo.service';
import {Guard} from './Guard';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    NewTodoComponent,
    TodoListComponent,
    TodoItemComponent,
    AboutContainer,
    TodoContainer
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    RouterModule.forRoot([
      {
        path: '', pathMatch: 'full', redirectTo: 'todo'
      },
      {
        path: 'todo', component: TodoContainer
      },
      {
        path: 'about', component: AboutContainer
      }
    ])
  ],
  bootstrap: [AppComponent],
  providers: [
    TodoService,
    Guard
  ]
})
export class AppModule {
}
