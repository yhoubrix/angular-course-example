import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-todo-list',
  template: `<ul>
    <li *ngFor='let todo of todos'>
      <app-todo-item [todo]='todo' (removeTodo)='removeTodo.emit($event)'></app-todo-item>
    </li>
  </ul>`
})
export class TodoListComponent {
  @Input() todos: Array<string>;
  @Output() removeTodo = new EventEmitter<string>();
}
