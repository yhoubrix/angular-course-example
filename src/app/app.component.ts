import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <header>
      <a routerLink='/todo'>Todo App</a>
      <a routerLink='/about'>About</a>
    </header>
    <div>
      <router-outlet></router-outlet>
    </div>
  `,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
}
