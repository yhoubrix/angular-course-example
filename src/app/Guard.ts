import {ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot} from '@angular/router';
import {TodoContainer} from './todo.container';
import {Observable} from 'rxjs/Observable';

export class Guard implements CanDeactivate<TodoContainer> {
  canDeactivate(component: TodoContainer, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return false;
  }

}
