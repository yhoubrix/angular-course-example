import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/share';

@Component({
  selector: 'app-about',
  template: `<h2>{{(person$ | async).name}}</h2>`
})
export class AboutContainer implements OnInit {
  person$: Observable<any>;

  constructor(private http: HttpClient) {

  }

  ngOnInit(): void {
    this.person$ = this.http.get('https://swapi.co/api/people/1/');
  }

}
