import {Injectable} from '@angular/core';

@Injectable()
export class TodoService {

  public todos = [];

  addTodoItem(value: string) {
    if (!this.todos.includes(value)) {
      this.todos = [...this.todos, value];
    }
  }

  removeTodo(todoToRemove) {
    this.todos = this.todos.filter((todo) => todo !== todoToRemove);
  }
}
