import {Component, OnInit} from '@angular/core';
import {TodoService} from './todo.service';

@Component({
  selector: 'app-todo',
  template: `
    <h1>TodoApp</h1>
    <app-new-todo (addTodoItem)='todoService.addTodoItem($event)'></app-new-todo>
    <app-todo-list [todos]='todoService.todos'
                   (removeTodo)='todoService.removeTodo($event)'></app-todo-list>
  `
})
export class TodoContainer {
  constructor(public todoService: TodoService) {
  }

}
