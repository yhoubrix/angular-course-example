import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-new-todo',
  template: `<input #todoItem
                    (keyup.enter)='onEnter(todoItem.value);todoItem.value="";'>`
})
export class NewTodoComponent {
  @Output() addTodoItem = new EventEmitter<string>();

  onEnter(value) {
    this.addTodoItem.emit(value);
  }
}
