import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-todo-item',
  template: `{{ todo }}
  <button (click)='removeTodo.emit(todo)'>Remove</button>`
})
export class TodoItemComponent {
  @Input() todo: string;
  @Output() removeTodo = new EventEmitter<string>();
}
